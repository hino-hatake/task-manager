# task-manager

An Application for executing, managing and monitoring scheduled tasks for RMHub system.

### 1. Technologies
- RESTful API
- Java 11
- Spring boot
- Spring data jpa + hibernate
- kafka
